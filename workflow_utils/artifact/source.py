from __future__ import annotations
from typing import TYPE_CHECKING, Optional, Generator

from abc import abstractmethod
import os
import contextlib
import fsspec
from fsspec import AbstractFileSystem

from workflow_utils.artifact.locator import ArtifactLocator
from workflow_utils.artifact.maven import maven_artifact_uri

if TYPE_CHECKING:
    # We only need to import this for type hints.
    # If we do imports when not checking types, we get a circular import.
    # See: https://stackoverflow.com/a/39757388
    # Note: Importing annotations from __future__ allows us to
    # refer to the Artifact type directly, instead of as a String.
    from workflow_utils.artifact import Artifact


class ArtifactSource(ArtifactLocator):
    """
    An ArtifactLocator that can open artifacts for reading.
    """
    @abstractmethod
    @contextlib.contextmanager
    def open(self, artifact: Artifact) -> Generator:
        """
        Yields a read only file-like IO stream object for artifact.

        :param artifact: Artifact
        """


class FsArtifactSource(ArtifactSource):
    """
    Source of artifacts retrievable via URIs.
    All protocols supported by fsspec are supported.
    E.g. local fs, HDFS, HTTP, etc.

    For FsArtifactSource, the artifact id should be a URI.

    :param base_uri:
        Base URI to prefix to all URI artifact_ids.
        If not provided, it is expected that artifact_ids
        are absolute URLs. If they aren't, an error will occur.
    """

    def __init__(self, base_uri: Optional[str] = None):
        self.base_uri = base_uri
        super().__init__()

    def __repr__(self) -> str:
        return f'{self.__class__.__name__}({self.base_uri or ""})'

    def fs(self, artifact: Artifact) -> AbstractFileSystem:  # pylint: disable=invalid-name
        """
        We don't keep a reference to the FileSystem used by
        this name.  The FileSystem can be obtained by calling
        fspec.open, which returns an OpenFile object, which
        has a reference to the FileSystem.
        """
        open_file = fsspec.open(self.url(artifact))
        source_fs = open_file.fs
        open_file.close()
        return source_fs

    def url(self, artifact: Artifact) -> str:
        """
        For FsArtifactSource, the url is the artifact_id prefixed
        with the source's base_uri.

        :param artifact: Artifact
        """
        if self.base_uri:
            return os.path.join(self.base_uri, artifact.id)
        return artifact.id

    @contextlib.contextmanager
    def open(self, artifact: Artifact) -> Generator:
        """
        :param artifact: Artifact
        """
        url = self.url(artifact)
        self.log.debug(f'Opening {repr(artifact)} source {url} for reading.')
        with fsspec.open(self.url(artifact)) as open_file:
            yield open_file

    def exists(self, artifact: Artifact) -> bool:
        """
        :param artifact_id: a URI
        """
        return bool(self.fs(artifact).exists(self.url(artifact)))

    def size(self, artifact: Artifact) -> Optional[int]:
        """
        :param artifact_id: a URI
        """
        try:
            size = int(self.fs(artifact).size(self.url(artifact)))
        except FileNotFoundError:
            size = None
        return size


class MavenArtifactSource(FsArtifactSource):
    """
    Source of artifacts in a Maven repository.
    This just translates between a Maven coordinate
    and a URL in a Maven repository, and uses
    FsArtifactSource to open the file.

    For MavenArtifactSource, the artifact_id should
    be a Maven coordinate.
    """
    def __init__(self, base_uri: str):
        """
        :param base_uri:
            Base URI to a Maven repository.  Required.
            E.g. https://archiva.wikimedia.org/repository/mirror-maven-central
        """
        super().__init__(base_uri=base_uri)

    def url(self, artifact: Artifact) -> str:
        """
        For MavenArtifactSource, the artifact_id is a Maven coordinate.

        :param artifact: Artifact.  artifact.id is A maven coordinate string.
        """
        # Make sure artifact_id looks like a coorindate.
        return maven_artifact_uri(artifact.id, str(self.base_uri))
