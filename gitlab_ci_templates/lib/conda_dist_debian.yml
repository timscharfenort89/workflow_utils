# Variables and a script snippet that will build and publish a conda dist env as a debian package.

include:
  - local: gitlab_ci_templates/lib/conda_dist.yml

variables:

  POSTINST_SCRIPT:
    description: >
      Add a debian post install shell script to run on the system when it installs the debian 
      package. Example: running conda-unpack.
      Notice: $PACKAGE_NAME is a replace by the variable defined by the CI, whereas $$ are converted
      to $. Keep in mind that the postinst script is run on a target system when the package 
      is installed. And it means that CI variables are not set.
    value: |
      #!/usr/bin/env bash
      set -e
      echo "Post install script."
      CONDA_PYTHON=$$(dpkg -L $PACKAGE_NAME | grep bin/python$$ | head -1)
      CONDA_UNPACK=$$(dpkg -L $PACKAGE_NAME | grep bin/conda-unpack$$ | head -1)
      if [ -z "$${CONDA_UNPACK}" ] ; then echo "ERROR CONDA_UNPACK is empty." ; exit 1 ; fi
      echo "  Running $$CONDA_PYTHON $$CONDA_UNPACK..."
      $$CONDA_PYTHON $$CONDA_UNPACK

# Script to perform a sanity check to make sure the package name is debian compatible.
.check_debian_package_name:
  - >
    (echo "$PACKAGE_NAME" | grep -Eq  "[_ ]") &&
    echo "ERROR: Invalid debian package name '${PACKAGE_NAME}' (space or underscore)" &&
    exit 1

.debian_build_setup:
  - !reference [.check_debian_package_name]
  - !reference [.conda_dist_build_script]
  - apt install -y fakeroot dpkg-dev curl ca-certificates git debhelper
  - DEBIAN_DIR=dist/debian
  - BUILD_DIR="${DEBIAN_DIR}/${PACKAGE_NAME}"
  - DEST_DIR="${BUILD_DIR}/opt/${PACKAGE_NAME}"
  - mkdir -p $DEST_DIR "${BUILD_DIR}/DEBIAN"
  - mv -t $DEBIAN_DIR debian/* conda* setup.cfg pyproject.toml
  - test -z "${PACKAGE_VERSION}" && PACKAGE_VERSION=$(${PACKAGE_VERSION_SCRIPT})
  - echo "${PACKAGE_VERSION}"
  - ARCH="$(dpkg --print-architecture)"
  - DEB_FILE="${PACKAGE_NAME}-${PACKAGE_VERSION}_${ARCH}.deb"
  - DEB_FILE_PATH=dist/$DEB_FILE
  - tar zxf dist/conda_dist_env.tgz -C "${DEST_DIR}/"
  - ls -lh /opt/conda/
  - rm /opt/conda/pkgs/*.{conda,tar.bz2}
  - mv /opt/conda/pkgs ${DEST_DIR}/pkgs
  - mkdir -p "${BUILD_DIR}/usr/bin"
  - >
    if [ ! -z "${POSTINST_SCRIPT}" ]; then 
    echo "${POSTINST_SCRIPT}" > postinst && 
    cat postinst && 
    mv postinst "${BUILD_DIR}/DEBIAN/" && 
    chmod 775 "${BUILD_DIR}/DEBIAN/postinst"; fi

# Creates a debian package for a conda environment.
# The installation path is: /opt/<your-package-name>
# The binaries will be linked to: /usr/bin/<your-bin>
.debian_build_script:
  - !reference [.debian_build_setup]
  - mkdir -p "${BUILD_DIR}/usr/bin"
  - cd dist
  - dh_installdirs
  - dh_install
  - dh_link
  - dpkg-gencontrol -P"../${BUILD_DIR}" -f"../${BUILD_DIR}/DEBIAN/files"
  - cd ..
  - fakeroot dpkg-deb --build $BUILD_DIR $DEB_FILE_PATH

# The debian package will be published to $PACKAGE_REGISTRY_URI.
.debian_publish_script:
  - !reference [.debian_build_script]
  - echo $CI_JOB_TOKEN
  - echo $GENERIC_PACKAGE_REGISTRY_URI
  - echo "${GENERIC_PACKAGE_REGISTRY_URI}/${PACKAGE_NAME}/${PACKAGE_VERSION}/${DEB_FILE}"
  - >
    curl
    --header "JOB-TOKEN: ${CI_JOB_TOKEN}"
    --upload-file "${DEB_FILE_PATH}"
    "${GENERIC_PACKAGE_REGISTRY_URI}/${PACKAGE_NAME}/${PACKAGE_VERSION}/${DEB_FILE}"
